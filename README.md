# CANGUSSU

Um E-Commerce destinado a Venda de Livros, Filmes e Jogos.


## Dupla:
 - Bruno Félix, 16/0114705;  
 - Carla Cangussu, 17/0085023;



## Execução do Programa:

### Requisitos Necessários para a Execução da Aplicação Web:

1. O Ruby on Rails:
* Versão do Ruby: 2.5.3;
* Versão do Rails: 5.2.1.


### No Terminal:

```
bundle install
rake db:migrate
rails s
```


